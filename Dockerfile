FROM debian:bookworm-slim

RUN apt-get update
RUN apt-get install -y --no-install-recommends git nodejs npm rsync
RUN rm -rf /var/lib/apt/lists/*
