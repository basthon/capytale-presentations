---
marp: true
title: Présentation Capytale Labo Math Issoudun
description: Présentation de Capytale au Lycée Balzac-D'Alembert à Issoudun (25/11/2022)
author: Romain Casati
theme: gaia
paginate: true
_paginate: false
---

<style>
section {
  background-color: #fefefe;
  color: #333;
}

img[alt~="center"] {
  display: block;
  margin: 0 auto;
}

blockquote {
  background: #ffedcc;
  border-left: 10px solid #d1bf9d;
  margin: 1.5em 10px;
  padding: 0.5em 10px;
}

blockquote:before{
  content: unset;
}

blockquote:after{
  content: unset;
}
</style>

<!-- _class: lead -->

# Capytale

Tout pour le codage au collège et au lycée

[![Capytale](./assets/logo.svg)](https://capytale.fr)

<style scoped>a { color: #aaa; }</style>

https://capytale.fr

Le 25/11/2022 au lycée Balzac-d'Alembert à Issoudun

<img alt="Logo lyc. BA" src="./assets/logo-ba.jpg" style="position: absolute; left: 0; bottom: 0; height: 3.5em;"/>

<img alt="Labomaths" src="./assets/labomaths.png" style="position: absolute; right: 0; top: 0; height: 2.5em;"/>

<img alt="logos académies" src="./assets/logo-ac.svg" style="position: absolute; left: 20px; top: 5px; height: 2.5em;"/>

<!--
Merci de m'acceuillir (Mathieu, le Labomaths), ...
Se présenter (double casquette : enseignant utilisateur et développeur)
Qui a déjà utilisé Capytale ? Avec ses élèves ?
-->

---

<!-- _class: lead -->

![QRCode height:10em](./assets/qrcode.svg)

<style scoped>a { color: #aaa; }</style>

https://capytale.basthon.fr/pres/ba1122

<!--
Cette présentation est à retrouver ici !
-->

---

## Déroulé de l'après-midi

### De 14h à 15h45 : présentation de Capytale
 - vue d'ensemble
 - le flux de travail enseignant ↔ élève
 - le déploiement sur le territoire et le fonctionnement
 - démonstration des activités
 - utiliser la bibliothèque
 - obtenir de l'aide
 - utilisations avancées

<!--
1h30 à 2h de présentation plutôt magistrale

Posez les questions au fur et à mesure
-->

---

## Déroulé de l'après-midi

### De 15h45 à 17h : mise en pratique
 - création d'une activité Capytale par les stagiaires
 - questions avancées et demandes spécifques

<!--
Pour ce second temps, utilisez vos postes ou ceux de la salle.
-->

---

## QQOQC Capytale

 - **Quoi ?** un service de création et le partage d’activités de codage
 - **Qui ?**  pour les enseignants et les élèves du second degré
 - **Où ?** hébergé par ac. de Paris, développé par Paris et Orléans-Tours
 - **Quand ?** depuis début 2020 et prévu pour durer !
 - **Comment ?** intégré aux ENT, 100% en ligne, sans installation, souverain

### <!--fit--> :thumbsup: Le meilleur moyen de travailler le codage avec les élèves

<!--
Indiquer les disciplines concernées :
 - Technologie
 - PC
 - SVT
 - SNT
 - Maths (maths-science)
 - NSI
 - SI
 - Informatique en CPGE
Modérer "le meilleur moyen"
-->

---

## Les activités dans Capytale

Du collège aux CPGE :

![Activités disponibles dans Capytale center height:5em](./assets/players.png)

<!--
Parcour exhaustif, on reviendra en détail sur chacun d'eux.

Ce n'est pas fini, plein d'autres arrivent !
-->

---

## Cycle de vie d'une activité dans Capytale

![Cycle de vie d'une activité center height:6em](./assets/echanges-prof-eleve.svg)

[![Live demo center height:6em](./assets/my-demo.png)](https://eduscol.education.fr/document/14044/download)

<!--
Présenter Codabloc : aller sur eduscol et faire le tracé du carré
Insister sur :
 - le code de partage
 - la boucle enseignant/élève
 - la vue prof, la correction
-->

---

![bg right:29% 100%](./assets/deploiement.png)

## En Octobre 2022, on compte

 - 100% des académies couvertes
 - 335k inscrits
 - 160k utilisateurs actifs
 - sur plus de 4k établissements
 - 960k activités
 - 130k activités consultées chaque semaine
 - dont 100k notebooks exécutés
 - +2,5k activités partagées entre enseignants
 - 100k activités créées depuis la bibliothèque.

<!--
Aller vite là dessus
-->

---

## Fonctionnement de Capytale

![Fonctionnement de Capytale center height:10em](./assets/connexion.png)

<!--
Souveraineté, vie privée, RGPD
-->

---

![bg right:60% 100%](./assets/vue-pixel-art.png)

### Pixel-Art

![center height:3em](./assets/pixel-art-logo.png)

---

![bg right:60% 100%](./assets/vue-codabloc.png)

### Codabloc

![center height:3em](./assets/codabloc-logo.png)

---

![bg right:60% 100%](./assets/vue-codepuzzle.gif)

### CodePuzzle

![center height:3em](./assets/codepuzzle-logo.png)

---

![bg right:60% 100%](./assets/vue-html.png)

### HTML / CSS / JS

![center height:3em](./assets/html-logo.png)

---

![bg right:60% 100%](./assets/vue-console.png)

### Script + Console Python

![center height:3em](./assets/console-logo.png)

---

![bg right:60% 100%](./assets/vue-notebook.png)

### Notebook Python

![center height:3em](./assets/notebook-logo.png)

---

![bg right:60% 100%](./assets/vue-sql.png)

### SQL

![center height:3em](./assets/sql-logo.png)

---

![bg right:60% 100%](./assets/vue-ocaml.png)

### OCaml

![center height:3em](./assets/ocaml-logo.png)

---

## La bibliothèque et le partage d'activités

![Bibliothèque Capytale center height:10em](./assets/bibliotheque.png)

<!--
Capytale c'est aussi une communautée d'enseignants qui construisent une bibliothèque d’activités partagées sous licence libre cc-by-sa
2500 activités partagées
Chacune est dupliquée 40 fois en moyenne

 - avec une activité Codabloc : taper "codabloc trace", pas satisfaisant puis "Bruno Marcant" ou juste "Marcant"
 - avec une activité Python en MS : taper pile face (parler de Magali SImon et Céline Le Fouler)
 - avec une activité Python en PC : python physique
 
Cloner l'activité et la proposer aux élèves

Présenter le partage d'une activité par un enseignant
-->

---

## Obtenir de l'aide

![bg right:35% 100%](./assets/wiki.png)

 - wiki :<br/>https://capytale2.ac-paris.fr/wiki
 - liste de diffusion :<br/>capytale@groupes.renater.fr
 - contacter les dev. :<br/>capytale-request@groupes.renater.fr
 - documentation de Basthon :<br/>https://basthon.fr/doc.html

---

## Utilisation avancée

 - organiser ses activités avec les tags
 - utiliser des fichiers annexes
 - intégrer une activité Capytale dans Moodle
 - évaluer avec Capytale (accès restreint dans le temps, auto-tests)
 - intégration d'éléments multimédia dans les consignes (applet GGB, image, vidéo, etc...)
 - partage d'activités entre enseignants (par lien, par association)
 - création d'une activité par un élève, travail en groupe
 - modules Python disponibles dans Capytale

<!--
 - tags : montrer sur mon compte
 - fichiers annexes : taper "photovoltaique" dans la bibliothèque
 - Moodle : wiki
 - évaluer : sur mon compte
 - Multimedia :
   - Geogebra : taper "integration geogebra" dans le bilbiothèque (Lionel Blin)
   - Vidéo : idem
   - Image dans la consigne avec un fichier annexe
 - modules Python : wiki
-->

---

<!-- _class: lead -->

#### <!--fit--> 😴 Merci pour votre attention ! 😴

