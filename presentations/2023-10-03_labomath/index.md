---
marp: true
title: Présentation Capytale Coordos Labo Math
description: Présentation de Capytale aux Coordos des Labos Math (03/10/2023)
author: Romain Casati
theme: gaia
paginate: true
_paginate: false
---

<style>
section {
  background-color: #fefefe;
  color: #333;
}

img[alt~="center"] {
  display: block;
  margin: 0 auto;
}

blockquote {
  background: #ffedcc;
  border-left: 10px solid #d1bf9d;
  margin: 1.5em 10px;
  padding: 0.5em 10px;
}

blockquote:before{
  content: unset;
}

blockquote:after{
  content: unset;
}
</style>

<!-- _class: lead -->

# Capytale

Tout pour le codage au collège et au lycée

[![Capytale](./assets/logo.svg)](https://capytale.fr)

<style scoped>a { color: #aaa; }</style>

https://capytale.fr

Le 03/10/2023

<img alt="Labomaths" src="./assets/labomaths.png" style="position: absolute; right: 0; top: 0; height: 2.5em;"/>

<img alt="logos académies" src="./assets/logo-ac.svg" style="position: absolute; left: 20px; top: 5px; height: 2.5em;"/>

<!--
Merci de m'acceuillir (Mathieu, le Labomaths), ...
Se présenter (double casquette : enseignant utilisateur et développeur)
Qui a déjà utilisé Capytale ? Avec ses élèves ?
-->

---

<!-- _class: lead -->

![QRCode height:10em](./assets/qrcode.svg)

<style scoped>a { color: #aaa; }</style>

https://capytale.basthon.fr/pres/lm1023

<!--
Cette présentation est à retrouver ici !
-->

---

## Déroulé des 20 minutes

- vue d'ensemble de Capytale
- le flux de travail enseignant ↔ élève
- démonstration des activités
- partager des activités
- obtenir de l'aide

---

## QQOQC Capytale

- **Quoi ?** un service de création et le partage d’activités de codage
- **Qui ?** pour les enseignants et les élèves du second degré
- **Où ?** hébergé par ac. de Paris, développé par Paris et Orléans-Tours
- **Quand ?** depuis début 2020 et prévu pour durer !
- **Comment ?** intégré aux ENT, 100% en ligne, sans installation, souverain

### <!--fit--> :thumbsup: Le meilleur moyen de travailler le codage avec les élèves

<!--
Indiquer les disciplines concernées :
 - Technologie
 - PC
 - SVT
 - SNT
 - Maths (maths-science)
 - NSI
 - SI
 - Informatique en CPGE
Modérer "le meilleur moyen"
-->

---

## Les activités dans Capytale

Du collège aux CPGE :

![Activités disponibles dans Capytale center height:10em](./assets/players.png)

<!--
Parcour exhaustif, on reviendra en détail sur chacun d'eux.

Ce n'est pas fini, plein d'autres arrivent !
-->

---

## Cycle de vie d'une activité dans Capytale

![Cycle de vie d'une activité center height:6em](./assets/echanges-prof-eleve.svg)

[![Live demo center height:6em](./assets/my-demo.png)](https://lycees.netocentre.fr)

<!--
Présenter Codabloc : aller sur eduscol et faire le tracé du carré
Insister sur :
 - le code de partage
 - la boucle enseignant/élève
 - la vue prof, la correction
-->

---

![bg right:60% 100%](./assets/vue-pixel-art.png)

### Pixel-Art

![center height:3em](./assets/pixel-art-logo.png)

---

![bg right:60% 100%](./assets/vue-codabloc.png)

### Codabloc

![center height:3em](./assets/codabloc-logo.png)

---

![bg right:60% 100%](./assets/vue-codepuzzle.gif)

### CodePuzzle

![center height:3em](./assets/codepuzzle-logo.png)

---

![bg right:60% 100%](./assets/vue-html.png)

### HTML / CSS / JS

![center height:3em](./assets/html-logo.png)

---

![bg right:60% 100%](./assets/vue-console.png)

### Script + Console Python

![center height:3em](./assets/console-logo.png)

---

![bg right:60% 100%](./assets/vue-notebook.png)

### Notebook Python

![center height:3em](./assets/notebook-logo.png)

---

![bg right:60% 100%](./assets/vue-geogebra.png)

### Geogebra

![center height:3em](./assets/geogebra-logo.svg)

---

![bg right:60% 100%](./assets/vue-mathalea.png)

### MathALÉA

![center height:3em](./assets/mathalea-logo.svg)

---

## La bibliothèque et le partage d'activités

![Bibliothèque Capytale center height:10em](./assets/bibliotheque.png)

<!--
Capytale c'est aussi une communautée d'enseignants qui construisent une bibliothèque d’activités partagées sous licence libre cc-by-sa
2500 activités partagées
Chacune est dupliquée 40 fois en moyenne

 - avec une activité Codabloc : taper "codabloc trace", pas satisfaisant puis "Bruno Marcant" ou juste "Marcant"
 - avec une activité Python en MS : taper pile face (parler de Magali SImon et Céline Le Fouler)
 - avec une activité Python en PC : python physique

Cloner l'activité et la proposer aux élèves

Présenter le partage d'une activité par un enseignant
-->

---

## Obtenir de l'aide

![bg right:35% 100%](./assets/wiki.png)

- wiki :<br/>https://capytale2.ac-paris.fr/wiki
- liste de diffusion :<br/>capytale@groupes.renater.fr
- contacter les dev. :<br/>capytale-request@groupes.renater.fr
- documentation de Basthon :<br/>https://basthon.fr/doc.html

---

<!-- _class: lead -->

#### <!--fit--> 😴 Merci pour votre attention ! 😴
