---
marp: true
title: Présentation Capytale APMEP
description: Présentation de Capytale au journée départementale de l'APMEP (20/01/2024)
author: Romain Casati
theme: gaia
paginate: true
_paginate: false
---

<style>
section {
  background-color: #fefefe;
  color: #333;
}

img[alt~="center"] {
  display: block;
  margin: 0 auto;
}

blockquote {
  background: #ffedcc;
  border-left: 10px solid #d1bf9d;
  margin: 1.5em 10px;
  padding: 0.5em 10px;
}

blockquote:before{
  content: unset;
}

blockquote:after{
  content: unset;
}
</style>

<!-- _class: lead -->

# <!--fit--> Présentation du service Capytale

Pour vos activités numériques et de programmation

[![Capytale](./assets/logo.svg)](https://capytale.fr)

<style scoped>a { color: #aaa; }</style>

https://capytale.fr

Journée départementale de l'APMEP — 20/01/2024

<img alt="APMEP" src="./assets/logo-apmep.png" style="position: absolute; right: 0; bottom: 0; width: 10%;"/>

<img alt="logos académies" src="./assets/logo-ac.svg" style="position: absolute; right: 20px; top: 5px; height: 2em;"/>

<!--
Se présenter (double casquette : enseignant utilisateur et développeur)
Faire un rapide tour de table pour cerner l'auditoire
-->

---

<!-- _class: lead -->

![QRCode](./assets/qrcode.png)

<style scoped>a { color: #aaa; }</style>

https://capytale.basthon.fr/pres/apmep0124

<!--
Cette présentation est à retrouver ici !
-->

---

## Déroulé des 45 minutes

- vue d'ensemble de Capytale
- le flux de travail enseignant ↔ élève
- démonstration des activités
- partager des activités
- obtenir de l'aide
- questions/réponses

---

## QQOQC Capytale

- **Quoi ?** un service de création et le partage d’activités num./prog.
- **Qui ?** pour les enseignants et les élèves du second degré
- **Où ?** hébergé par ac. de Paris, développé par Paris et Orléans-Tours
- **Quand ?** depuis début 2020 et prévu pour durer !
- **Comment ?** intégré aux ENT, 100% en ligne, sans installation, souverain

### <!--fit--> :thumbsup: Un très bon moyen de travailler le codage avec les élèves

<!--
Indiquer les disciplines concernées :
 - Technologie
 - PC
 - SVT
 - SNT
 - Maths
 - NSI
 - SI
 - Informatique en CPGE
Modérer "le meilleur moyen"
-->

---

## Les activités dans Capytale

Du collège aux CPGE :

![Activités disponibles dans Capytale center height:10em](./assets/players.png)

---

## Les activités dans Capytale

Du collège aux CPGE :

![Activités disponibles dans Capytale center height:10em](./assets/players-math.png)

<!--
Essayer de cerner ici l'activité qui intéresse le plus les participants pour savoir sur quoi faire la démo
-->

---

## Cycle de vie d'une activité dans Capytale

![Cycle de vie d'une activité center height:6em](./assets/echanges-prof-eleve.svg)

[![Live demo center height:6em](./assets/my-demo.png)](https://eduscol.education.fr/document/14044/download)

<!--
Présenter au choix : Console, Codabloc, Notebook
Insiter sur :
 - le code de partage
 - la boucle enseignant/élève
 - la vue prof, la correction
-->

---

![bg right:29% 100%](./assets/deploiement.png)

## Fin 2023, on compte

- 100% des académies couvertes
- sur plus de 5k établissements
- 2700k activités
- 125k activités consultées chaque semaine
- dont 100k notebooks exécutés
- +4k activités partagées entre enseignants
- 200k activités créées depuis la bibliothèque.

<!--
Aller vite là dessus
-->

---

![bg right:60% 100%](./assets/vue-codabloc.png)

### Codabloc

![center height:3em](./assets/codabloc-logo.png)

---

![bg right:60% 100%](./assets/vue-console.png)

### Script + Console Python

![center height:3em](./assets/console-logo.png)

---

![bg right:60% 100%](./assets/vue-notebook.png)

### Notebook Python

![center height:3em](./assets/notebook-logo.png)

---

![bg right:60% 100%](./assets/vue-geogebra.png)

### Geogebra

![center height:3em](./assets/geogebra-logo.svg)

---

![bg right:60% 100%](./assets/vue-mathalea.png)

### MathALÉA

![center height:3em](./assets/mathalea-logo.svg)

---

## La bibliothèque et le partage d'activités

![Bibliothèque Capytale center height:10em](./assets/bibliotheque.png)

<!--
Capytale c'est aussi une communautée d'enseignants qui construisent une bibliothèque d’activités partagées sous licence libre cc-by-sa
2500 activités partagées
Chacune est dupliquée 40 fois en moyenne
Aller dans la bibliothèque et taper "bases"
Cloner le notebook et le proposer à ses élèves
Dire un mot sur le partage d'une activité par un enseignant
-->

---

## Ressources

![bg right:35% 100%](./assets/wiki.png)

- wiki :<br/>https://capytale2.ac-paris.fr/wiki
- liste de diffusion :<br/>capytale@groupes.renater.fr
- contacter les dev. :<br/>capytale-request@groupes.renater.fr
- documentation de Basthon :<br/>https://basthon.fr/doc.html

---

## Utilisation avancée

- organiser ses activités avec les tags
- intégrer une activité Capytale dans Moodle
- évaluer avec Capytale (accès restreint dans le temps, auto-tests)
- intégration d'éléments multimédia dans les consignes (applet GGB, image, vidéo, etc...)
- partage d'activités entre enseignants (par lien, par association)
- création d'une activité par un élève, travail en groupe
- modules Python disponibles dans Capytale
- auto-évaluation de l'élève

---

#### <!--fit--> :grey_question::grey_question::grey_question:

---

## Fonctionnement de Capytale

![Fonctionnement de Capytale center height:10em](./assets/connexion.png)

<!--
Souveraineté, vie privée, RGPD
GAR ?
-->
