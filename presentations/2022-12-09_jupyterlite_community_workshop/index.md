---
marp: true
title: Basthon presentation to JupyterLite Community Workshop 2022
description: Basthon presentation to JupyterLite Community Workshop 2022 [Paris]
author: Romain Casati
theme: gaia
paginate: true
_paginate: false
---

<style>
section {
  background-color: #fefefe;
  color: #333;
}

img[alt~="center"] {
  display: block;
  margin: 0 auto;
}

blockquote {
  background: #ffedcc;
  border-left: 10px solid #d1bf9d;
  margin: 1.5em 10px;
  padding: 0.5em 10px;
}

blockquote:before{
  content: unset;
}

blockquote:after{
  content: unset;
}
</style>

<!-- _class: lead -->

[![Basthon height:5em](./assets/basthon-logo.svg)](https://basthon.fr)
#### Client-side Notebook&Console
#### Python/SQL/OCaml kernels

targeted for french high schools

</br>

<style scoped>a { color: #aaa; }</style>
https://basthon.fr — romain.casati@basthon.fr

JupyterLite Community Workshop 2022 [Paris]

<img alt="logos académies" src="./assets/logo-ac.svg" style="position: absolute; left: 20px; top: 5px; height: 2.5em;"/>
<img alt="logos capytale" src="./assets/capytale-logo.svg" style="position: absolute; right: 20px; top: 5px; height: 2.5em;"/>

<!--
-->

---

<!-- _class: lead -->

![QRCode height:10em](./assets/qrcode.svg)

<style scoped>a { color: #aaa; }</style>

https://capytale.basthon.fr/pres/jlcw22

<!--
-->

---

# History

 - dec. 2019: project started (based on Brython at that time)
 - march-may 2020: used with my students
 - summer 2020: switched to Pyodide
 - summer 2020: Jupyter Classic Notebook hacked to be fully client-side
 - oct. 2020: Capytale integration started
 - now: +100k Basthon-based activity opened/week in Capytale

---

![center height:15em](https://basthon.fr/theme/assets/img/galerie/python3/python3-spirale.png)

---

![center height:15em](https://basthon.fr/theme/assets/img/galerie/python3/python3-turtle-writing.png)

---

## Targeted for high school

 - Turtle support (animated SVG from Brython)
 - Graphviz (using viz.js: Graphviz compiled to WASM)
 - p5.js bindings
 - input / sleep
 - PythonTutor
 - requests
 - QRCode, pyroutelib3, ipythonblocks, lolviz, rcviz, binarytree, ...
 - Matplotlib HTML5 (x/y coordinates matters!) + animations
 - desktop version for macOS/Windows/GNU Linux/Android

---

[![center height:15em](./assets/gallery.png)](https://basthon.fr/galerie.html)

---

## Cons

 - main thread (need DOM manipulation for p5, matplotlib)
 - old notebook (updated deps. + webpack5)
 - in french
 - only one active dev 😥

---

## Many thanks to...

 - Pyodide community
 - Jupyter community
 - Capytale/Basthon users
 - Python/Emscripten/...
 
---
 
## The future of Basthon

 - use JupyterLite/Notebook 7
 - use comlink/via.js
 - focus on build a Python kernel/distribution targeted for high schools

---

<!-- _class: lead -->

#### <!--fit--> 😴 Thanks! 😴

