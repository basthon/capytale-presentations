---
marp: true
title: Présentation Capytale Labo Math Pithiviers
description: Présentation de Capytale au Lycée Duhamel du Monceau (13/06/2024)
author: Romain Casati
theme: gaia
paginate: true
_paginate: false
---

<style>
section {
  background-color: #fefefe;
  color: #333;
}

img[alt~="center"] {
  display: block;
  margin: 0 auto;
}

blockquote {
  background: #ffedcc;
  border-left: 10px solid #d1bf9d;
  margin: 1.5em 10px;
  padding: 0.5em 10px;
}

blockquote:before{
  content: unset;
}

blockquote:after{
  content: unset;
}
</style>

<!-- _class: lead -->

# Capytale

Activités numériques et de codage au collège et au lycée

[![Capytale](./assets/logo.svg)](https://capytale.fr)

<style scoped>a { color: #aaa; }</style>

https://capytale.fr

Lycée Duhamel du Monceau à Pithiviers

13/06/2024

<img alt="Logo lyc. DDM" src="./assets/logo-etab.png" style="position: absolute; left: 0; bottom: 0; height: 3.5em;"/>

<img alt="Labomaths" src="./assets/labomaths.png" style="position: absolute; right: 0; top: 0; height: 2.5em;"/>

<img alt="logos académies" src="./assets/logo-ac.svg" style="position: absolute; left: 20px; top: 5px; height: 2.5em;"/>

<!--
Merci de m'acceuillir (le Labomaths), ...
Se présenter (double casquette : enseignant utilisateur et développeur)
Qui a déjà utilisé Capytale ? Avec ses élèves ?

Mathaléa, Geogebra, Autoevaluation, Notebook séquencé
En fin de pres, montrer la bibliothèque pour faire le lien avec la deuxième partie.
-->

---

<!-- _class: lead -->

![QRCode height:10em](./assets/qrcode.svg)

<style scoped>a { color: #aaa; }</style>

https://capytale.basthon.fr/pres/lddm0624

<!--
Cette présentation est à retrouver ici !
-->

---

## Résultat du sondage

- 10 réponses
- 100% mathématiques
- 60% lycée
- 70% n'ont pas encore utilisé Capytale

---

## Déroulé de la matinée

### De 9h à 10h30 : découverte de Capytale

- vue d'ensemble du service
- le flux de travail enseignant ↔ élève
- découverte de quelques activités
- utiliser la bibliothèque
- obtenir de l'aide
- questions/réponses

<!--
Posez les questions au fur et à mesure
-->

---

## Déroulé de la matinée

### De 10h30 à 12h : prise en main

- création d'une activité Capytale par les stagiaires
- utilisation des ressources de la bibliothèque
- questions avancées et demandes spécifques

<!--
Pour ce second temps, utilisez vos postes ou ceux de la salle.
-->

---

## QQOQC Capytale

- **Quoi ?** un service de création et le partage d’activités num./prog.
- **Qui ?** pour les enseignants et les élèves du second degré
- **Où ?** hébergé par ac. de Paris, développé par Paris et Orléans-Tours
- **Quand ?** depuis début 2020 et prévu pour durer !
- **Comment ?** intégré aux ENT, 100% en ligne, sans installation, souverain

### <!--fit--> :thumbsup: Un très bon moyen de travailler le codage avec les élèves

---

## Les activités dans Capytale

Du collège aux CPGE :

![Activités disponibles dans Capytale center height:10em](./assets/players.png)

---

## Les activités dans Capytale

Du collège aux CPGE :

![Activités disponibles dans Capytale center height:10em](./assets/players-math.png)

<!--
Essayer de cerner ici l'activité qui intéresse le plus les participants pour savoir sur quoi faire la démo
-->

---

## Cycle de vie d'une activité dans Capytale

![Cycle de vie d'une activité center height:6em](./assets/echanges-prof-eleve.svg)

[![Live demo center height:6em](./assets/my-demo.png)](https://eduscol.education.fr/document/14044/download)

---

![bg right:29% 100%](./assets/deploiement.png)

## En juin 2024, on compte

- 100% des académies couvertes
- 430k utilisateurs actifs
- sur plus de 8k établissements
- 4M d'activités
- 125k activités consultées chaque semaine
- +5k activités partagées entre enseignants
- 200k activités créées depuis la bibliothèque

<!--
Aller vite là dessus
-->

---

![bg right:60% 100%](./assets/vue-codabloc.png)

### Codabloc

![center height:3em](./assets/codabloc-logo.png)

---

![bg right:60% 100%](./assets/vue-console.png)

### Script + Console Python

![center height:3em](./assets/console-logo.png)

---

![bg right:60% 100%](./assets/vue-notebook.png)

### Notebook Python

![center height:3em](./assets/notebook-logo.png)

---

![bg right:60% 100%](./assets/vue-geogebra.png)

### Geogebra

![center height:3em](./assets/geogebra-logo.svg)

<!--
Parler de la paramétrisation de la barre d'outils
-->

---

![bg right:50% 100%](./assets/vue-mathalea.png)

### MathALÉA

![center height:3em](./assets/mathalea-logo.svg)

[Prise en main](https://capytale2.ac-paris.fr/wiki/doku.php?id=mathalea)

---

![bg right:50% 100%](./assets/vue-notebook.png)

### Notebook séquencés (Python)

![center height:3em](./assets/notebook-logo.png)

[Prise en main](https://capytale2.ac-paris.fr/wiki/doku.php?id=notebook_sequence)

---

## La bibliothèque et le partage d'activités

![Bibliothèque Capytale center height:13em](./assets/bibliotheque.png)

<!--
Capytale c'est aussi une communautée d'enseignants qui construisent une bibliothèque d’activités partagées sous licence libre cc-by-sa
2500 activités partagées
Chacune est dupliquée 40 fois en moyenne

 - avec une activité Codabloc : taper "codabloc trace", pas satisfaisant puis "Bruno Marcant" ou juste "Marcant"
 - avec une activité Python en MS : taper pile face (parler de Magali Simon et Céline Le Fouler)
 - avec une activité Python en PC : python physique

Cloner l'activité et la proposer aux élèves

Présenter le partage d'une activité par un enseignant
-->

---

## Obtenir de l'aide

![bg right:35% 100%](./assets/wiki.png)

- wiki : https://capytale2.ac-paris.fr/wiki
- liste de diffusion :<br/>capytale@groupes.renater.fr
- contacter les dev. :<br/>capytale-request@groupes.renater.fr
- documentation de Basthon :<br/>https://basthon.fr/doc.html

---

## Utilisation avancée

- organiser ses activités avec les tags
- intégrer une activité Capytale dans Moodle
- évaluer avec Capytale (accès restreint dans le temps, auto-tests)
- intégration d'éléments multimédia dans les consignes (applet GGB, image, vidéo, etc...)
- partage d'activités entre enseignants (par lien, par association)
- création d'une activité par un élève, travail en groupe
- modules Python disponibles dans Capytale
- auto-évaluation de l'élève

---

#### <!--fit--> :grey_question::grey_question::grey_question:

---

## Fonctionnement de Capytale

![Fonctionnement de Capytale center height:10em](./assets/connexion.png)

<!--
Souveraineté, vie privée, RGPD
GAR ?
-->
<!-- _class: lead -->

#### <!--fit--> 😴 Merci pour votre attention ! 😴
