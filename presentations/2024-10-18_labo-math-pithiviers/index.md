---
marp: true
title: Présentation Capytale Labo Math Pithiviers
description: Présentation de Capytale au Lycée Duhamel du Monceau (18/10/2024)
author: Romain Casati
theme: gaia
paginate: true
_paginate: false
---

<style>
section {
  background-color: #fefefe;
  color: #333;
}

img[alt~="center"] {
  display: block;
  margin: 0 auto;
}

blockquote {
  background: #ffedcc;
  border-left: 10px solid #d1bf9d;
  margin: 1.5em 10px;
  padding: 0.5em 10px;
}

blockquote:before{
  content: unset;
}

blockquote:after{
  content: unset;
}
</style>

<!-- _class: lead -->

# Capytale

Activités numériques et de codage au collège et au lycée

[![Capytale](./assets/logo.svg)](https://capytale.fr)

<style scoped>a { color: #aaa; }</style>

<https://capytale.fr>

Lycée Duhamel du Monceau à Pithiviers

18/10/2024

<img alt="Logo lyc. DDM" src="./assets/logo-etab.png" style="position: absolute; left: 0; bottom: 0; height: 3.5em;"/>

<img alt="Labomaths" src="./assets/labomaths.png" style="position: absolute; right: 0; top: 0; height: 2.5em;"/>

<img alt="logos académies" src="./assets/logo-ac.svg" style="position: absolute; left: 20px; top: 5px; height: 2.5em;"/>

<!--
Merci de m'acceuillir (le Labomaths), ...
Se présenter (double casquette : enseignant utilisateur et développeur)
Qui a déjà utilisé Capytale ? Avec ses élèves ?

Mathaléa, Geogebra, Autoevaluation, Notebook séquencé
En fin de pres, montrer la bibliothèque pour faire le lien avec la deuxième partie.
-->

---

<!-- _class: lead -->

![QRCode height:10em](./assets/qrcode.svg)

<style scoped>a { color: #aaa; }</style>

<https://capytale.basthon.fr/pres/lddm1024>

<!--
Cette présentation est à retrouver ici !
-->

---

## Déroulé de la matinée

### De 9h à 10h30 : Découverte et approfondissement

- vue d'ensemble du service
- le flux de travail enseignant ↔ élève
- découverte de quelques activités
- utiliser la bibliothèque
- obtenir de l'aide
- utilisation avancée 🚀

<!--
Posez les questions au fur et à mesure
-->

---

## Déroulé de la matinée

### De 10h30 à 12h : prise en main

- création d'une activité Capytale par les stagiaires débutants
- utilisation des ressources de la bibliothèque
- questions avancées et demandes spécifiques 🚀

<!--
Pour ce second temps, utilisez vos postes ou ceux de la salle.
-->

---

## Les activités dans Capytale

Du collège aux CPGE :

![Activités disponibles dans Capytale center height:10em](./assets/players.png)

---

## Les activités dans Capytale

Du collège aux CPGE :

![Activités disponibles dans Capytale center height:10em](./assets/players-math.png)

<!--
Essayer de cerner ici l'activité qui intéresse le plus les participants pour savoir sur quoi faire la démo
-->

---

## Cycle de vie d'une activité dans Capytale

![Cycle de vie d'une activité center height:5em](./assets/echanges-prof-eleve.svg)

[![Live demo center height:7em](./assets/my-demo.png)](https://eduscol.education.fr/document/14044/download)

<div style="text-align: center; widht: 100%;">🚀</div>

---

![bg right:29% 100%](./assets/deploiement.png)

## En septembre 2024, on compte

- 100% des académies couvertes
- 430k utilisateurs actifs
- sur plus de 8k établissements
- 4M d'activités
- 125k activités consultées chaque semaine
- +5k activités partagées entre enseignants
- 200k activités créées depuis la bibliothèque

<!--
Aller vite là dessus
-->

---

![bg right:50% 100%](./assets/vue-codabloc.png)

### Codabloc

![center height:3em](./assets/codabloc-logo.png)

- [Prise en main](https://capytale2.ac-paris.fr/wiki/doku.php?id=codabloc)

---

![bg right:50% 100%](./assets/vue-console.png)

### Script + Console Python

![center height:3em](./assets/console-logo.png)

- [Prise en main](https://capytale2.ac-paris.fr/wiki/doku.php?id=script-console)

---

![bg right:50% 100%](./assets/vue-notebook.png)

### Notebook Python

![center height:3em](./assets/notebook-logo.png)

- [Prise en main](https://capytale2.ac-paris.fr/wiki/doku.php?id=notebook)
- [Version séquencé](https://capytale2.ac-paris.fr/wiki/doku.php?id=notebook_sequence) 🚀

---

![bg right:50% 100%](./assets/vue-geogebra.png)

### Geogebra

![center height:3em](./assets/geogebra-logo.svg)

- [Prise en main](https://capytale2.ac-paris.fr/wiki/doku.php?id=geogebra)
- [Mode anti-triche](https://capytale2.ac-paris.fr/wiki/doku.php?id=le_mode_anti-triche) 🚀

<!--
Parler de la paramétrisation de la barre d'outils
-->

---

![bg right:50% 100%](./assets/vue-mathalea.png)

### MathALÉA

![center height:3em](./assets/mathalea-logo.svg)

- [Prise en main](https://capytale2.ac-paris.fr/wiki/doku.php?id=mathalea)
- Beaucoup de nouveautés pour le lycée 🚀
- [Mode anti-triche](https://capytale2.ac-paris.fr/wiki/doku.php?id=le_mode_anti-triche) 🚀

---

## La bibliothèque et le partage d'activités

![Bibliothèque Capytale center height:13em](./assets/bibliotheque.png)

<!--
Capytale c'est aussi une communautée d'enseignants qui construisent une bibliothèque d’activités partagées sous licence libre cc-by-sa
2500 activités partagées
Chacune est dupliquée 40 fois en moyenne

 - avec une activité Codabloc : taper "codabloc trace", pas satisfaisant puis "Bruno Marcant" ou juste "Marcant"
 - avec une activité Python en MS : taper pile face (parler de Magali Simon et Céline Le Fouler)
 - avec une activité Python en PC : python physique

Cloner l'activité et la proposer aux élèves

Présenter le partage d'une activité par un enseignant
-->

---

## Obtenir de l'aide

![bg right:35% 100%](./assets/wiki.png)

- wiki : <https://capytale2.ac-paris.fr/wiki>
- [salon tchap](https://tchap.gouv.fr/#/room/#CapytalerxTiiB0hKQ:agent.education.tchap.gouv.fr) 🚀

- liste de diffusion :<br/><capytale@groupes.renater.fr>
- contacter les dev. :<br/><capytale-request@groupes.renater.fr>
- documentation de Basthon :<br/><https://basthon.fr/doc.html>

---

## Utilisation avancée 🚀

- [organiser ses activités avec les tags](https://capytale2.ac-paris.fr/wiki/doku.php?id=etiquettes)
- [archiver les travaux des élèves](https://capytale2.ac-paris.fr/wiki/doku.php?id=afficher-masquer)
- [intégrer une activité Capytale dans Moodle](https://capytale2.ac-paris.fr/wiki/doku.php?id=moodle_iframe)
- restreindre l'accès à une copie (évaluation)
- [partage d'activités entre enseignants](https://capytale2.ac-paris.fr/wiki/doku.php?id=partager_avec_un_associe)
- [création d'une activité par un élève, travail en groupe](https://capytale2.ac-paris.fr/wiki/doku.php?id=eleves_en_autonomie)
- dessiner avec Python (Turtle)
- [mode anti-triche](https://capytale2.ac-paris.fr/wiki/doku.php?id=le_mode_anti-triche)
- [notebook séquencé](https://capytale2.ac-paris.fr/wiki/doku.php?id=notebook_sequence)

---

#### <!--fit--> :grey_question::grey_question::grey_question:

<!-- _class: lead -->

---

#### <!--fit--> 😴 Merci pour votre attention ! 😴

<!-- _class: lead -->
